/**
 * @File: leader.hpp
 * @Date: 30 Jan 2020
 * @Author: Brian Merrell
 *
 * @brief
 * Contains virtual leader and structure components
 **/

#ifndef LEADER_HPP
#define LEADER_HPP

// ROS headers
#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "nav_msgs/Odometry.h"
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include "nav_msgs/Path.h"
#include "std_msgs/Bool.h"
#include <visualization_msgs/Marker.h>

// Library headers
#include <Eigen/Dense>

// Local headers
// #include "virtual_structure/BetterUnicycle.hpp"
#include "param_opt/BetterUnicycle.hpp"
#include "voronoi_generator/voronoi_generator.hpp"
#include "cc_planner/cc_planner.hpp"

class Leader
{
public:
  
  //Default Constructor
  Leader() = delete;

  Leader(const Eigen::Matrix2Xd &formation_config, const Eigen::Matrix2Xd &path_waypoints, const int &agent_number,
                    const double &dt, const double &velocity_nominal, const double &curvature_max, const double &curvature_rate);

  // Copy Constructor
  Leader(const Leader&) = delete;
  // Move Constructor
  Leader(Leader&&) = delete;
  // Default Deconstructor
  ~Leader();
  // Copy Assignment Operator
  Leader& operator=(const Leader&);
  // Move Assignment Operator
  Leader& operator=(Leader&&);

  void odomCallback(const nav_msgs::Odometry::ConstPtr & msg);
  void publishTrajectory();
  void visualizeVoronoi();
  void publishCommand();
  void publishStart();


  Eigen::Matrix2Xd formation;
  Eigen::Matrix2Xd path_waypoints;
  int agent_number;
  double dt;
  double velocity_nominal;
  double curvature_max;
  double curvature_rate;
  
  VoronoiGenerator voronoi_structure;
  std::shared_ptr<ccp::traj::LeaderTrajectory> trajectory;
  std::shared_ptr<ccp::traj::FollowerTrajectory> followerTraj;
  std::shared_ptr<ccp::traj::Trajectory> epsilonTraj;
  BetterUnicycle vehicle;
  int n_agents;
  double k_v;
  double k_w;
  double time_start;

  private:
  // ROS variables
  ros::NodeHandle nh;
  ros::Publisher pub_command;
  ros::Publisher pub_path;
  ros::Publisher pub_voronoi;
  ros::Publisher pub_start;
  ros::Subscriber sub_goal;
  ros::Subscriber sub_odom;
  tf::TransformListener tf_listener;

  // Message storage variables
  geometry_msgs::Twist cmd;
  geometry_msgs::PoseStamped goal;
  nav_msgs::Odometry::ConstPtr odom;
  std_msgs::Bool sim_start;

  // Flags
  bool goal_received; // True if a goal location has been received, false otherwise

    
    
  

};






#endif