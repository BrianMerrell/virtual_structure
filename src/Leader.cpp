
#include "virtual_structure/Leader.hpp"
// #include "param_opt/BetterUnicycle.hpp"
#include<iostream>
#include<vector>


Leader::Leader(const Eigen::Matrix2Xd &formation_config, const Eigen::Matrix2Xd &path_waypoints, const int &agent_number,
                    const double &dt, const double &velocity_nominal, const double &curvature_max, const double &curvature_rate)     
 : formation(formation_config),
   path_waypoints(path_waypoints),
   agent_number(agent_number),
   dt(dt),
   velocity_nominal(velocity_nominal),
   curvature_max(curvature_max),
   curvature_rate(curvature_rate),
   trajectory(ccp::makeLeaderTrajectory(path_waypoints.colwise() + this->formation.col(agent_number),velocity_nominal, dt, curvature_max, curvature_rate, 50)),
   followerTraj(ccp::makeFollowerTrajectory(*trajectory,this->formation.col(agent_number))),
   epsilonTraj(ccp::makeEpislonTrajectory(*trajectory, 0.2)),
   vehicle(this->trajectory, this->followerTraj, this->epsilonTraj, dt, agent_number, formation),
   n_agents(this->formation.cols()-1),
   time_start(0.0)  
{
    // std::vector<double> x0;
    // std::vector<double> waypoints;
    // Read node parameters
    // ros::NodeHandle nh("~");
    // nh.getParam("initial_state", x0);
    // nh.getParam("trans_vel_gain", k_v);
    // nh.getParam("rot_vel_gain", k_w);
    // nh.getParam("n_agents", n_agents);

    
    // Build voronoi diagram
    this->voronoi_structure.addAgents(formation.rightCols(this->n_agents));
    this->voronoi_structure.construct<1000>();

    // Initialize the vehicle position
    std::vector<double> x0{this->trajectory->cgetPositionVec()(0,0),
                           this->trajectory->cgetPositionVec()(1,0),
                           this->trajectory->cgetYawVec()(0,0),
                           0,
                           0};
    this->vehicle.x0 = x0;
    
    cmd.linear.y = 0.0;
    cmd.linear.z = 0.0;
    cmd.linear.x = 0.0;

    cmd.angular.x = 0.0;
    cmd.angular.y = 0.0;
    cmd.angular.z = 0.0;

    pub_path = nh.advertise<nav_msgs::Path>("cctraj", 1);
    pub_command = nh.advertise<geometry_msgs::Twist>("cmd_vel", 1);
    pub_voronoi = nh.advertise<visualization_msgs::Marker>("voronoi", 10);
    pub_start = nh.advertise<std_msgs::Bool>("start", 1);

    sub_odom = nh.subscribe("odom", 1, &Leader::odomCallback, this);

}

Leader::~Leader(){

}


void Leader::odomCallback(const nav_msgs::Odometry::ConstPtr &msg){
    odom = msg;
}

void Leader::publishTrajectory(){
    if(odom){
    // Create geometry msg container
    geometry_msgs::PoseStamped traj_msg;
    traj_msg.header.frame_id =  odom->header.frame_id;;
    traj_msg.header.stamp = ros::Time::now();
    // traj_msg.pose.position.x = 0.0;
    // traj_msg.pose.position.y = 0.0;
    traj_msg.pose.position.z = 0.0;
    traj_msg.pose.orientation = tf::createQuaternionMsgFromYaw(0.0);

    // Create path msg container
    nav_msgs::Path traj;
    traj.header.stamp = ros::Time::now();
    traj.header.frame_id =  odom->header.frame_id;;

    // Fill path msg with geometry msgs
    for(uint i = 0; i < trajectory->length(); i++){
        traj_msg.pose.position.x = trajectory->cgetPositionVec()(0,i);
        traj_msg.pose.position.y = trajectory->cgetPositionVec()(1,i);
        traj_msg.pose.orientation = tf::createQuaternionMsgFromYaw(trajectory->cgetYawVec()(i));
        traj.poses.push_back(traj_msg);
    }
    // Publish
    pub_path.publish(traj);
    }
}

void Leader::visualizeVoronoi()
{
    if(odom)
    {
        
        visualization_msgs::Marker line_list;
        line_list.header.frame_id = odom->header.frame_id;
        line_list.header.stamp = ros::Time(0);
        line_list.type = line_list.LINE_LIST;
        line_list.action = line_list.ADD;
        line_list.pose.orientation.w = 1.0;

        line_list.id = 0;
        line_list.scale.x = 0.04;

        line_list.color.b = 1.0;
        line_list.color.a = 1.0;

        // Set Time and Index
        double time_now = ros::Time::now().toSec() - time_start;
        int indAtTime = trajectory->findIndex(time_now);
        // Get pose at now
        Eigen::Vector2d positionAtTimeNow = trajectory->cgetPositionVec().col(indAtTime);
        double yawAtTimeNow = trajectory->cgetYawVec()(indAtTime);

        // Get pose of current
        voronoi_structure.translate(yawAtTimeNow, positionAtTimeNow);
        std::vector<std::shared_ptr<Eigen::Matrix2d>> barrier = this->voronoi_structure.cgetEdges();

        for(std::size_t i = 0; i < barrier.size(); i++)
        {
            geometry_msgs::Point vertex1;
            vertex1.x = (*barrier.at(i))(0,0);
            vertex1.y = (*barrier.at(i))(1,0);
            vertex1.z = 0;

            geometry_msgs::Point vertex2;
            vertex2.x = (*barrier.at(i))(0,1);
            vertex2.y = (*barrier.at(i))(1,1);
            vertex2.z = 0;
            
            line_list.points.push_back(vertex1);
            line_list.points.push_back(vertex2);
        }
        voronoi_structure.translate(-yawAtTimeNow, -positionAtTimeNow);
    pub_voronoi.publish(line_list);
    // std::shared_ptr<std::vector<double>> dist = this->voronoi_structure.getDistances(formation.block<2,1>(0,1),formation.block<2,1>(0,1));
    
    // auto cell = voronoi_structure.findCell(formation.block<2,1>(0,1)).lock();
    // for(auto it = cell->cbegin(); it != cell->cend(); it++)
    // {
    //   std::cout << *it << std::endl;
    // }
    // std::cout << std::endl;
    }
}

void Leader::publishCommand()
{   
    if(odom){
        if(this->time_start == 0.0)
        {
            this->time_start = ros::Time::now().toSec();
        }
    double time = ros::Time::now().toSec() - this->time_start;
    // std::cout << time << std::endl;
    
    std::vector<double> x(5);
    x[0] = odom->pose.pose.position.x;              // x position
    x[1] = odom->pose.pose.position.y;              // y position
    x[2] = tf::getYaw(odom->pose.pose.orientation); // psi orientation
    x[3] = odom->twist.twist.linear.x;              // v translational velocity
    x[4] = odom->twist.twist.angular.z;             // w rotational velocity



    Eigen::Matrix<double, 2, 1> epsilonCommand = this->vehicle.epsilonTrajectoryTrackingController(time, x, this->epsilonTraj);
    cmd.linear.x = x.at(3)+epsilonCommand(0,0)*this->dt;
    cmd.angular.z = x.at(4)+epsilonCommand(1,0)*this->dt;

    // double velAtTimeNow = trajectory->cgetVelocity();
    // double angularVelAtTimeNow = trajectory->angularVelAt(time);
    // cmd.linear.x = velAtTimeNow;
    // cmd.angular.z = angularVelAtTimeNow;
    pub_command.publish(cmd);
    }
}

void Leader::publishStart()
{   
    if(odom){
        sim_start.data = true;
        pub_start.publish(sim_start);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "leader");
    ros::NodeHandle nh_local("~");
    ros::start();
    
    // ros::Time start(0.0000001);
    // ros::Time::setNow(start);

    // Get ROS params
    int agent_number;
    double dt, velocity_nominal, curvature_max, curvature_rate;
    std::vector<double> x_formation, y_formation, x_path, y_path;
    nh_local.getParam("x_formation", x_formation);
    nh_local.getParam("y_formation", y_formation);
    nh_local.getParam("x_path", x_path);
    nh_local.getParam("y_path", y_path);
    nh_local.getParam("agent_number", agent_number);
    nh_local.getParam("dt", dt);
    nh_local.getParam("velocity_nominal", velocity_nominal);
    nh_local.getParam("curvature_max", curvature_max);
    nh_local.getParam("curvature_rate", curvature_rate);

    // Create matrix for the formation
    std::vector<double>::iterator x_iter, y_iter;
    x_iter = x_formation.begin();
    y_iter = y_formation.begin();
    int n = x_formation.size();
    Eigen::Matrix2Xd formation = Eigen::Matrix2Xd::Zero(2,n);
    for(std::size_t iter = 0; iter < n; iter++)
    {
        formation(0,iter) = *x_iter;
        formation(1,iter) = *y_iter;
        x_iter++; y_iter++;
    }

    // Create matrix for the path waypoints
    x_iter = x_path.begin();
    y_iter = y_path.begin();
    n = x_path.size();
    Eigen::Matrix2Xd path_waypoints = Eigen::Matrix2Xd::Zero(2,n);
    for (std::size_t iter = 0; iter < n; iter++)
    {   
        path_waypoints(0,iter) = *x_iter;
        path_waypoints(1,iter) = *y_iter;
        x_iter++; y_iter++;
    }

    ros::Rate r(1.0/dt);
    ros::Rate r_start(.2);

    // Construct Leader
    // double time_start = ros::Time::now().toSec();
    Leader virtual_leader(formation, path_waypoints, agent_number, dt, velocity_nominal, curvature_max, curvature_rate);
    // 14ms to create Leader
    // std::cout<< time_start - ros::Time::now().toSec() << std::endl;

    

    r_start.sleep();

    while(ros::ok()) {
        ros::spinOnce();
        virtual_leader.publishStart();
        virtual_leader.publishTrajectory();
        virtual_leader.publishCommand();
        virtual_leader.visualizeVoronoi();
        r.sleep();
    }
    return 0;

}


